import { DirectiveOptions } from 'vue'
import VueTooltip from '@/components/general/Tooltip.vue'
import { DirectiveBinding } from 'vue/types/options'

interface HTMLTooltipElement extends HTMLElement {
  tooltip?: VueTooltip;
}

class Tooltip implements DirectiveOptions {
  bind(el: HTMLElement, binding: DirectiveBinding): void {
    const tooltip = new VueTooltip({
      propsData: { text: binding.value }
    })
    tooltip.$mount()

    const element = el as HTMLTooltipElement
    element.tooltip = tooltip
    element.classList.add('tooltip-parent')
    element.appendChild(tooltip.$el)
  }

  unbind(el: HTMLElement): void {
    const element = el as HTMLTooltipElement
    element.classList.remove('tooltip-parent')
    if (element.tooltip) {
      element.removeChild(element.tooltip.$el)
      element.tooltip.$destroy()
      element.tooltip = undefined
    }
  }
}

export default new Tooltip()

declare module 'vue-context'

declare class VueContext extends Vue {
  open(event: unknown, data: unknown): void
}

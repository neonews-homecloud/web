import displayToastOnError from './displayToastOnError'
import { authenticationGuard } from './authentication'

export default (vue: Vue, block: () => Promise<void>, errorMessage: () => string): Promise<void> => {
  return displayToastOnError(vue, () => authenticationGuard(vue, block), errorMessage)
}

import Vue from 'vue'
import VueI18n, { LocaleMessages } from 'vue-i18n'
import { language } from './locale'

Vue.use(VueI18n)

const loadLocaleMessages = (): LocaleMessages => {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  return locales.keys()
    .filter(key => {
      const matched = key.match(/([A-Za-z0-9-_]+)\./i)
      return matched && matched.length > 1
    })
    .reduce((map: LocaleMessages, key: string) => {
      const matched = key.match(/([A-Za-z0-9-_]+)\./i)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const locale = matched![1]
      map[locale] = locales(key)
      return map
    }, {})
}

export default new VueI18n({
  locale: language(),
  fallbackLocale: 'en',
  messages: loadLocaleMessages(),
  silentFallbackWarn: true
})

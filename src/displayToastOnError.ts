export default async (vue: Vue, block: () => Promise<void>, errorMessage: () => string): Promise<void> => {
  try {
    await block()
  } catch (error) {
    vue.$toast.error(errorMessage())
  }
}

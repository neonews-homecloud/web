export const language = (): string => {
  return navigator.language.slice(0, 2).toLocaleLowerCase()
}

import Installation from '@/components/Installation.vue'
import Login from '@/components/Login.vue'
import FileBrowser from '@/components/FileBrowser.vue'

export const routes = [
  { path: '/installation', component: Installation },
  { path: '/login', component: Login },
  { path: '/*', component: FileBrowser }
]

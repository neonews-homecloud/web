export abstract class DirectoryItem {
  filename: string
  basename: string
  lastmod: Date

  protected constructor (filename: string, basename: string, lastmod: Date) {
    this.filename = filename
    this.basename = basename
    this.lastmod = lastmod
  }
}

export class Directory extends DirectoryItem {
  constructor (filename: string, basename: string, lastmod: Date) {
    super(filename, basename, lastmod)
  }
}

export class File extends DirectoryItem {
  size: number
  mime: string

  constructor (filename: string, basename: string, lastmod: Date, size: number, mime: string) {
    super(filename, basename, lastmod)
    this.size = size
    this.mime = mime
  }

  isImage(): boolean {
    return this.mime.startsWith('image/')
  }
}

import Vue, { VNode } from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueToast from 'vue-toast-notification'
import VueHotkey from 'v-hotkey'
import App from '@/App.vue'
import { routes } from '@/routes'
import createStore from '@/store'
import i18n from '@/i18n'
import 'typicons.font/src/font/typicons.css'
import 'vue-context/src/sass/vue-context.scss'
import 'vue-toast-notification/dist/theme-default.css'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueToast)
Vue.use(VueHotkey)

const router = new VueRouter({
  mode: 'history',
  routes
})

// Set page title. Source: https://stackoverflow.com/questions/51639850/how-to-change-page-titles-when-using-vue-router
const DEFAULT_TITLE = 'Homecloud'
router.afterEach((to) => {
  // Use next tick to handle router history correctly
  // see: https://github.com/vuejs/vue-router/issues/914#issuecomment-384477609
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})

new Vue({
  router,
  store: createStore(),
  i18n,
  render: (h): VNode => h(App)
}).$mount('#app')

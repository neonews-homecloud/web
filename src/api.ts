const postJson = async (api: string, object: unknown): Promise<void> => {
  const url = `${process.env.VUE_APP_API_URL}${api}`
  const response = await fetch(url, {
    credentials: 'same-origin',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(object)
  })

  if (!response.ok) {
    throw new Error(`Error on postJson ${api}: ${response.status}`)
  }
}

export const authenticate = async (name: string, password: string): Promise<void> => postJson('/api/authenticate', { name, password })

export const signUp = async (name: string, password: string): Promise<void> => postJson('/api/users', { name, password })

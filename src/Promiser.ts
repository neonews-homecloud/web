export default class <T> {
  done: boolean
  success: boolean
  failure: boolean

  constructor (promise: Promise<T> | null) {
    this.done = !promise
    this.success = false
    this.failure = false

    if (promise) {
      promise
        .then(() => { this.success = true })
        .catch(() => { this.failure = true })
        .finally(() => { this.done = true })
    }
  }
}

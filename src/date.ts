import { format as formatFns, formatISO, formatDistanceToNow, parseISO as parseISOFns } from 'date-fns'
import { de, enUS } from 'date-fns/locale'
import { language } from '@/locale'

const locales: Record<string, Locale> = { enUS, de }
const locale = locales[language()]

export const parseISO = (value: string): Date => { return parseISOFns(value) }

export const toISO = <T>(value: T | undefined): string => {
  if (!value) {
    return ''
  }
  if (!(value instanceof Date)) {
    return ''
  }
  return formatISO(value)
}

export const format = <T>(value: T | undefined): string => {
  if (!value) {
    return ''
  }
  if (!(value instanceof Date)) {
    return ''
  }
  return formatFns(value, 'PPpp', { locale })
}

export const timeago = (value: Date | undefined): string => {
  if (!value) {
    return ''
  }
  return formatDistanceToNow(value, { locale })
}

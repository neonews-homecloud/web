import { createClient } from 'webdav/web'
import { DirectoryItem, Directory, File } from './DirectoryItem'

export class AuthenticationError extends Error {
  constructor(message: string) {
    super(message)
  }
}

export default class {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  client: any

  constructor(url: string) {
    this.client = createClient(url)
  }

  async list(directory: string): Promise<DirectoryItem[]> {
    try {
      return (await this.client.getDirectoryContents(directory, {
        headers: {
          Authorization: 'invalid'
        }
      }))
        .map((item: { type: string; filename: string; basename: string; lastmod: string; size: number; mime: string }) => {
          if (item.type === 'file') {
            return new File(item.filename, item.basename, new Date(item.lastmod), item.size, item.mime)
          } else if (item.type === 'directory') {
            return new Directory(item.filename, item.basename, new Date(item.lastmod))
          } else {
            throw Error('Couldn\'t parse directory items')
          }
        })
        .sort(this.sort)
    } catch (error) {
      // See here: https://github.com/perry-mitchell/webdav-client/blob/v4.3.0/source/response.ts#L9
      if (error.status === 401 || error.message === 'Request failed with status code 401') {
        throw new AuthenticationError(error.message)
      } else {
        throw error
      }
    }
  }

  async move(source: string, destination: string): Promise<void> {
    await this.client.moveFile(source, destination)
  }

  private sort(a: DirectoryItem, b: DirectoryItem): number {
    if (a instanceof Directory && !(b instanceof Directory)) {
      return -1
    } else if (b instanceof Directory && !(a instanceof Directory)) {
      return 1
    }
    return a.filename.localeCompare(b.filename)
  }
}

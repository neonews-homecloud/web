import { AuthenticationError } from './webdav'

export const authenticationGuard = async (vue: Vue, block: () => Promise<void>): Promise<void> => {
  try {
    await block()
  } catch (error) {
    if (error instanceof AuthenticationError) {
      vue.$router.push('login')
    } else {
      throw error
    }
  }
}

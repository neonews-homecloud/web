import { Store, StoreOptions } from 'vuex'
import { DirectoryItem, File, Directory } from '@/DirectoryItem'
import Promiser from '@/Promiser'
import WebDAV from '@/webdav'
import path from 'path-browserify'

type State = {
  directory: string;
  directoryItems: DirectoryItem[];
  listPromiser: Promiser<void>;
}

const webdavUrl = `${process.env.VUE_APP_API_URL}/files`
const client = new WebDAV(webdavUrl)

export const store = (): StoreOptions<State> => ({
  state: {
    directory: '',
    directoryItems: [],
    listPromiser: new Promiser(null)
  },
  mutations: {
    SET_DIRECTORY(state: State, directory: string): void {
      state.directory = directory
    },
    SET_DIRECTORY_ITEMS(state: State, items: DirectoryItem[]): void {
      state.directoryItems = items
    },
    SET_LIST_PROMISER(state: State, promiser: Promiser<void>): void {
      state.listPromiser = promiser
    },
    RENAME(state: State, payload: { item: DirectoryItem; newFilename: string; newBasename: string }): void {
      const iteminStore = state.directoryItems.find((it) => it.filename === payload.item.filename)
      if (iteminStore) {
        iteminStore.filename = payload.newFilename
        iteminStore.basename = payload.newBasename
      }
    }
  },
  actions: {
    async listDirectory({ commit }, directory: string): Promise<void> {
      commit('SET_DIRECTORY_ITEMS', [])
      commit('SET_DIRECTORY', directory)
      const promise = client.list(directory)
      commit('SET_LIST_PROMISER', new Promiser(promise))
      commit('SET_DIRECTORY_ITEMS', await promise)
    },
    async rename({ commit }, payload: { item: DirectoryItem; newName: string }): Promise<void> {
      const oldPath = path.parse(payload.item.filename)
      const newFilename = path.format({
        ...oldPath,
        base: payload.newName,
        ext: null,
        name: null
      })
      if (payload.item.filename === newFilename) {
        return
      }
      await client.move(payload.item.filename, newFilename)
      commit('RENAME', { item: payload.item, newFilename, newBasename: payload.newName })
    }
  },
  getters: {
    fileCount: (state: State): number =>
      state.directoryItems.filter(item => item instanceof File).length,
    directoryCount: (state: State): number =>
      state.directoryItems.filter(item => item instanceof Directory).length
  }
})

export default (): Store<State> => new Store(store())

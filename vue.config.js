// eslint-disable-next-line @typescript-eslint/no-var-requires
const StyleLintPlugin = require('stylelint-webpack-plugin')

module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  css: {
    extract: false,
    loaderOptions: {
      scss: {
        additionalData: `
          @import "@/scss/theme.scss";
        `
      }
    }
  },
  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    performance: {
      maxEntrypointSize: 512000,
      maxAssetSize: 512000
    }
  },
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Homecloud'
        return args
      })
  }
}

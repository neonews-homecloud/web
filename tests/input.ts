import { fireEvent } from '@testing-library/vue'

const text = (input: HTMLElement): HTMLInputElement => {
  return input.querySelector('input') || fail('Couldn\'t find input element')
}

export const update = async (input: HTMLElement, value: string): Promise<void> => {
  await fireEvent.update(text(input), value)
}

export const enter = async (input: HTMLElement): Promise<void> => {
  await fireEvent.keyDown(text(input), { key: 'Enter', code: 'Enter' })
}

export const submit = async (input: HTMLElement): Promise<void> => {
  await fireEvent.submit(text(input))
}

import tooltip from '@/directives/tooltip'

describe('tooltip', () => {
  let element: HTMLElement

  beforeEach(() => {
    element = document.createElement('span')
    tooltip.bind(element, {
      name: 'Test',
      value: 'This is the tooltip!',
      modifiers: {}
    })
  })

  it('binds', async () => {
    expect(element.className).toEqual('tooltip-parent')
    expect(element.children).toHaveLength(1)

    const child = element.firstChild as HTMLElement
    expect(child.tagName).toEqualCaseInsensitive('span')
    expect(child.className).toEqual('tooltip')
    expect(child).toHaveTextContent('This is the tooltip!')
  })

  it('unbinds', async () => {
    tooltip.unbind(element)

    expect(element.className).toBeEmpty()
    expect(element.children).toBeEmpty()
  })
})

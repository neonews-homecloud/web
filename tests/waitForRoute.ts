import { waitFor } from '@testing-library/vue'

export default async (route: string): Promise<void> => {
  return waitFor(() => expect(window.location.href).toEqual(route))
}

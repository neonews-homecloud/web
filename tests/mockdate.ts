import MockDate from 'mockdate'
import { parseISO } from '@/date'

MockDate.set(parseISO('2020-01-30T15:00:00+01:00'))

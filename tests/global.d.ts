// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { Server } from 'net'

declare global {
  namespace NodeJS {
    interface Global {
      __MOCKSERVER__: Server;
    }
  }
}

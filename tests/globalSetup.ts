import http from 'http'
import mockserver from 'mockserver'

export default async (): Promise<void> => {
  console.log('Starting mock server at port 9001')
  const server = http.createServer(mockserver('./mocks'))
  await new Promise((resolve) => { server.listen(9001, () => { resolve(undefined) }) })
  global.__MOCKSERVER__ = server
}

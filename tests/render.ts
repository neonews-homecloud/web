import { render, RenderOptions, RenderResult } from '@testing-library/vue'
import { VueClass } from '@vue/test-utils'
import VueI18n from 'vue-i18n'
import VueToast from 'vue-toast-notification'
import VueHotkey from 'v-hotkey'
import { routes } from '@/routes'
import { store } from '@/store'
import Vue, { VueConstructor } from 'vue'
import { StoreOptions } from 'vuex'

export default <V extends Vue>(component: VueClass<V>, options?: RenderOptions<V>, initialRoute?: string): RenderResult => {
  return render(component, { routes, store: store() as unknown as StoreOptions<Record<string, unknown>>, ...options }, (vue, _, router) => {
    const _vue = vue as unknown as VueConstructor
    _vue.use(VueI18n)
    _vue.use(VueHotkey)
    _vue.use(VueToast)
    _vue.$toast.clear()

    if (initialRoute) {
      router.push(initialRoute)
    }

    return {
      i18n: new VueI18n({
        locale: 'en',
        silentTranslationWarn: true
      }
      )
    }
  })
}

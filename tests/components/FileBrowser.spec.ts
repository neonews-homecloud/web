import FileBrowser from '@/components/FileBrowser.vue'
import { fireEvent, waitForElementToBeRemoved } from '@testing-library/vue'
import { update, enter } from '../input'
import render from '../render'

describe('FileBrowser', () => {
  it('lists all files and directories', async () => {
    const { findAllByTestId } = render(FileBrowser)
    const files = await findAllByTestId(/^file-\d+-name/)

    expect(files).toHaveLength(3)

    const emptySubdir = files[0].querySelector('a') || fail('filename not found')
    expect(emptySubdir).toHaveTextContent('empty-subdir')

    const testSubdir = files[1].querySelector('a') || fail('filename not found')
    expect(testSubdir).toHaveTextContent('test-subdir')

    const file = files[2].querySelector('span') || fail('filename not found')
    expect(file).toHaveTextContent('test-file.txt')
  })

  it('can download a file', async () => {
    const { findByTestId } = render(FileBrowser)
    const actionButton = await findByTestId('file-2-actions')
    fireEvent.click(actionButton)

    const downloadLink = await findByTestId('download-link')
    expect(downloadLink).toHaveAttribute('href', 'http://localhost:9001/files/test-file.txt')
  })

  it('can rename a file', async () => {
    const { findByTestId } = render(FileBrowser)
    const actionButton = await findByTestId('file-2-actions')
    fireEvent.click(actionButton)

    const renameLink = await findByTestId('rename-link')
    fireEvent.click(renameLink)

    const renameInput = await findByTestId('editable-input')
    await update(renameInput, 'test-renamed-file.txt')
    await enter(renameInput)
    await waitForElementToBeRemoved(renameInput)

    const filename = await findByTestId('file-2-name')
    expect(filename.querySelector('span')).toHaveTextContent('test-renamed-file.txt')
  })

  it('cannot rename a file (Server responds HTTP 403)', async () => {
    const { findByTestId, getByText } = render(FileBrowser)
    const actionButton = await findByTestId('file-1-actions')
    fireEvent.click(actionButton)

    const renameLink = await findByTestId('rename-link')
    fireEvent.click(renameLink)

    const renameInput = await findByTestId('editable-input')
    await update(renameInput, 'test-renamed-dir')
    await enter(renameInput)
    await waitForElementToBeRemoved(renameInput)

    getByText('error.rename')
    const filename = await findByTestId('file-1-name')
    expect(filename.querySelector('a')).toHaveTextContent('test-subdir')
  })

  it('won\'t rename a file, if the filename didn\'t change', async () => {
    const { findByTestId, queryByText } = render(FileBrowser)
    const actionButton = await findByTestId('file-1-actions')
    fireEvent.click(actionButton)

    const renameLink = await findByTestId('rename-link')
    fireEvent.click(renameLink)

    const renameInput = await findByTestId('editable-input')
    await update(renameInput, 'test-subdir')
    await enter(renameInput)
    await waitForElementToBeRemoved(renameInput)

    expect(queryByText('error.rename')).toBeNull()
    const filename = await findByTestId('file-1-name')
    expect(filename.querySelector('a')).toHaveTextContent('test-subdir')
  })
})

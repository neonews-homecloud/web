import Installation from '@/components/Installation.vue'
import { update, submit } from '../input'
import render from '../render'
import waitForRoute from '../waitForRoute'

describe('Installation', () => {
  it('succeeds', async () => {
    const { findByTestId } = render(Installation, {}, '/installation')
    const name = await findByTestId('name')
    await update(name, 'user')

    const password = await findByTestId('password')
    await update(password, 'password')
    await submit(password)

    await waitForRoute('http://localhost/#/')
  })
})

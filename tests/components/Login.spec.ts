import Login from '@/components/Login.vue'
import { update, submit } from '../input'
import render from '../render'
import waitForRoute from '../waitForRoute'

describe('Login', () => {
  it('succeeds', async () => {
    const { findByTestId } = render(Login, {}, '/login')
    const name = await findByTestId('name')
    await update(name, 'user')

    const password = await findByTestId('password')
    await update(password, 'password')
    await submit(password)

    await waitForRoute('http://localhost/#/')
  })

  it('fails on invalid password', async () => {
    const { findByTestId, findByText } = render(Login, {}, '/login')
    const name = await findByTestId('name')
    await update(name, 'user')

    const password = await findByTestId('password')
    await update(password, 'invalid')
    await submit(password)

    await findByText('error.login')
    await waitForRoute('http://localhost/#/login')
  })
})

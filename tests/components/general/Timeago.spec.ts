import Timeago from '@/components/general/Timeago.vue'
import render from '../../render'

describe('Timeago', () => {
  it('show correct dates', async () => {
    const { findByText } = render(Timeago, {
      props: {
        datetime: '2020-02-20T21:00:00Z'
      }
    })

    const element = await findByText('21 days')
    expect(element.querySelector('.tooltip')).toHaveTextContent('Feb 20, 2020, 10:00:00 PM')
  })
})

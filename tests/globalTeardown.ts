export default async (): Promise<void> => {
  console.log('Stopping mock server')
  await new Promise((resolve) => { global.__MOCKSERVER__.close(resolve) })
}

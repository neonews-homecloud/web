# web

## Project setup
```
npm install
```

You might need to add `--legacy-peer-deps` on a recent npm, see [here](https://github.com/vuejs/vue-cli/issues/6270).

Create file `.env.development.local`

```
VUE_APP_API_URL=http://localhost:8001
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Fake API for testing
```
npm run api
```

### Compiles and minifies for production
```
npm run build
```

With the latest version of node, you might need to set `NODE_OPTIONS=--openssl-legacy-provider`, see [here](https://stackoverflow.com/questions/69394632/webpack-build-failing-with-err-ossl-evp-unsupported).

### Lints and fixes files
```
npm run lint
```

### Test
```
npm run test
```

### Storybook
```
npm run storybook:serve
```

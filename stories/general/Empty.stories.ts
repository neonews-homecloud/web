import { storiesOf } from '@storybook/vue'
import Empty from '@/components/general/Empty.vue'
import 'typicons.font/src/font/typicons.css'

storiesOf('Empty', module)
  .add('default', () => ({
    components: { Empty },
    template: '<empty icon="typcn typcn-folder" title="Empty folder" />'
  }))

import { storiesOf } from '@storybook/vue'
import LoadingBar from '@/components/general/LoadingBar.vue'

storiesOf('LoadingBar', module)
  .add('default', () => ({
    components: { LoadingBar },
    template: `<loading-bar :loading="true" style="margin: 10px; padding: 10px; border: 3px solid black">
    <div style="background-color: lightyellow">
      Loading Bar
    </div>
  </loading-bar>`
  }))
  .add('No border', () => ({
    components: { LoadingBar },
    template: `<loading-bar :loading="true">
    <div style="background-color: lightyellow">
      Loading Bar
    </div>
  </loading-bar>`
  }))
  .add('Not loading', () => ({
    components: { LoadingBar },
    template: `<loading-bar :loading="false">
    <div style="background-color: lightyellow">
      Loading Bar
    </div>
  </loading-bar>`
  }))

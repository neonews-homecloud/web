import { storiesOf } from '@storybook/vue'
import Loading from '@/components/general/Loading.vue'

storiesOf('Loading', module)
  .add('default', () => ({
    components: { Loading },
    template: '<loading />'
  }))
  .add('color', () => ({
    components: { Loading },
    template: '<loading style="color: orange" />'
  }))
  .add('large (font-size)', () => ({
    components: { Loading },
    template: '<loading style="font-size: 300%" />'
  }))
  .add('large (class)', () => ({
    components: { Loading },
    template: '<loading class="loading-large" />'
  }))

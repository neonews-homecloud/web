import { storiesOf } from '@storybook/vue'
import MyInput from '@/components/general/Input.vue'
import 'typicons.font/src/font/typicons.css'

storiesOf('Input', module)
  .add('default', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" />'
  }))
  .add('Placeholder', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" placeholder="Test-Placeholder" />'
  }))
  .add('Placeholder & Icon', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" placeholder="Test-Placeholder" icon="typcn typcn-arrow-sorted-up" />'
  }))
  .add('Loading', () => ({
    components: { MyInput },
    data: () => ({
      text: 'Test'
    }),
    template: '<my-input v-model="text" placeholder="Loading" loading="true" />'
  }))
  .add('Password', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" type="password" icon="typcn typcn-key" placeholder="Password" />'
  }))
  .add('Large font & Placeholder', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" placeholder="Test-Placeholder" style="font-size: 300%" />'
  }))
  .add('Large font & Loading', () => ({
    components: { MyInput },
    data: () => ({
      text: 'Test'
    }),
    template: '<my-input v-model="text" placeholder="Test-Placeholder" loading="true" style="font-size: 300%" />'
  }))
  .add('Large font & Placeholder & Icon', () => ({
    components: { MyInput },
    data: () => ({
      text: ''
    }),
    template: '<my-input v-model="text" placeholder="Test-Placeholder" icon="typcn typcn-arrow-sorted-up" style="font-size: 300%" />'
  }))
